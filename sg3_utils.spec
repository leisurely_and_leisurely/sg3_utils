Name:    sg3_utils
Version: 1.42
Release: 11
Summary: Utilities that send SCSI commands to devices.
License: GPL-2.0-or-later AND BSD
URL:     http://sg.danny.cz/sg/sg3_utils.html
Source0: http://sg.danny.cz/sg/p/sg3_utils-%{version}.tar.xz

Patch0000: 0000-sg3_utils-1.37-dont-open-dev-snapshot.patch
Patch0001: 0001-sg3_utils-1.42-sysmacros.patch

Patch9000: 9000-sg3_utils-1.37-rescan-scsi-findremapped-enhance.patch
Patch9001: 9001-sg3_utils-1.37-rescan-downpress.patch
Patch9002: 9002-bugfix-sg3_utils-fix-syntax-error.patch
Patch9003: 9003-sg_inq-fix-potential-unbounded-loop-in-export.patch
Patch0002: 0002-rescan-scsi-bus-dolunscan-break-to-return.patch
Patch9004: 9004-sg3_utils-1.42-delete-lun-rescan-scsi-bus-report-error.patch 

Provides:      %{name}-libs = %{version}-%{release}
Obsoletes:     %{name}-libs < %{version}-%{release}
BuildRequires: gcc, git

%description
The sg3_utils package contains utilities that send SCSI commands to devices. As well as
devices on transports traditionally associated with SCSI (e.g. Fibre Channel (FCP),
Serial Attached SCSI (SAS) and the SCSI Parallel Interface(SPI)) many other devices use
SCSI command sets. ATAPI cd/dvd drives and SATA disks that connect via a translation
layer or a bridge device are examples of devices that use SCSI command sets.

%package devel
Summary: A collection of tools that send SCSI commands to devices
Requires:    %{name}-libs

%description  devel
This subpackage contains libraries and header files for developing
applications that want to make use of libsgutils.

%package  help
Summary: Including man files for pciutils
Requires: man

%description  help
This contains man files for the using of pciutils.

%prep
%autosetup -Sgit -n %{name}-%{version}

%build
%configure --disable-static
# Don't use rpath!
%disable_rpath

make %{?_smp_mflags}

%install
%make_install
install -p -m 644 doc/rescan-scsi-bus.sh.8  $RPM_BUILD_ROOT%{_mandir}/man8
install -p -m 755 scripts/rescan-scsi-bus.sh $RPM_BUILD_ROOT%{_bindir}
( cd $RPM_BUILD_ROOT%{_bindir}; ln -sf rescan-scsi-bus.sh scsi-rescan )
rm -rf $RPM_BUILD_ROOT/%{_libdir}/*.la

#Files list
%files
%defattr(-,root,root)
%doc AUTHORS BSD_LICENSE COPYING COVERAGE CREDITS ChangeLog README README.sg_start
%{_bindir}/*
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/scsi/*.h
%{_libdir}/*.so

%files help
%{_mandir}/man8/*

%changelog
* Thu Apr 09 2020 Jiangping Hu <hujp1985@foxmail.com> - 1.42-11
- Fix requires of devel package

* Fri Mar 13 2020 wubo <wubo40@huawei.com> - 1.42-10
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESCi: modify rescan-scsi-bus dolunscan break to return

* Tue Sep 24 2019 wubo <wubo40@huawei.com> - 1.42-9
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESCi: delete lun rescan scsi bus report error

* Fri Aug 23 2019 zoujing <zoujing13@huawei.com> - 1.42-8
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESCi:openEuler Debranding

* Wed Aug 21 2019 wubo <wubo40@huawei.com> - 1.42-6.7
- change patch name

* Thu Jul 18 2019 wangye<wangye54@huawei.com> - 1.42-6.6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: huawei for dolunscan

* Mon Jul 8 2019 wangye<wangye54@huawei.com> - 1.42-6.5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: huawei break to return

* Tue Apr 16 2019 zhangyujing<zhangyujing1@huawei.com> - 1.42-6.4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:sg inq fix potential unbounded loop in export

* Tue Apr 2 2019 guyue<guyue7@huawei.com> - 1.42-6.3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete useless code

* Wed Mar 20 2019 wangjufeng<wangjufeng@huawei.com> - 1.42-6.2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix syntax error

* Wed Jan 23 2019 huangchangyu<huangchangyu@huawei.com> - 1.42-6.1
- Type:enhanced
- ID:NA
- SUG:NA
- DESC:change patch name according to new rule

* Tue Jan 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.42-6
- Package init
